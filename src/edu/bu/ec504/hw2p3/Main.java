package edu.bu.ec504.hw2p3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.HashSet;

public class Main {

    private static final String DELIMITER = "---"; // delimiter between input sections

    public static void main(String[] args) {
	      FindSimilar myFindSimilar = new MyFindSimilar();

        // thanks ChatGPT for the idea of how to convert a string into a BufferedReader
        String testStr = """
            oast
            oath
            ogre
            onyx
            oss
            ---
            ores
            ---
            """;
        StringReader strReader = new StringReader(testStr);

	      // Read and process input
        BufferedReader reader =
            new BufferedReader(strReader);
        try {
            // read in dictionary
            for (
                String line = reader.readLine();
                !line.equals(DELIMITER);
                line = reader.readLine())
            {
                myFindSimilar.addToDictionary(line);
            }

            // execute search on each word, one by one
            for (
                String line = reader.readLine();
                !line.equals(DELIMITER);
                line = reader.readLine())
            {
                int result = myFindSimilar.closestWord(line);
                System.out.println("Response: "+result);
                if (result!=2)
                    throw new RuntimeException("Incorrect return value - expecting 2.");
            }


        } catch (IOException e) {
            throw new RuntimeException("Something went wrong: "+e);
        }


    }

    // FIELDS
    HashSet<String> dict;  // The dictionary against which to check
}
